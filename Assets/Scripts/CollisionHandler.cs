﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;//ok as long as this is the only script that loads scenes

public class CollisionHandler : MonoBehaviour {

    [Tooltip("In Seconds")] [SerializeField] float levelLoadDelay = 3f;
    [Tooltip("FX Prefab on player")] [SerializeField] GameObject deathFX;

    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
    }

    private void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");
        deathFX.SetActive(true);

        Invoke("ReloadScene", levelLoadDelay);

    }

    private void ReloadScene() // String Reference
    {
        SceneManager.LoadScene(1);
    }
}

