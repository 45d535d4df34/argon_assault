﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    [Header("General")]
    [Tooltip("In ms^")] [SerializeField] float controlSpeed = 4f;
    [Tooltip("In m")] [SerializeField] float xRange = 5f;
    [Tooltip("In m")] [SerializeField] float yMax = 2f;
    [Tooltip("In m")] [SerializeField] float yMin = -2f;
    [SerializeField] GameObject[] guns;

    [Header("Screen Position Based")]
    [SerializeField] float positionPitchFactor = -5f;
    [SerializeField] float positionYawFactor = 5f;

    [Header("Control Throw Based")]
    [SerializeField] float controlPitchFactor = -20;
    [SerializeField] float controlRollFactor = -20f;

    bool isControlEnabled = true;
  

    float xThrow, yThrow;


    void OnPlayerDeath()//Called by string reference
    {
        isControlEnabled = false;
    }
 
    // Update is called once per frame
    void Update ()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFireing();
        }

    }



    private void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControl= yThrow * controlPitchFactor;
        float pitch= pitchDueToPosition+ pitchDueToControl;

        float yaw=transform.localPosition.x*positionYawFactor;

        float roll = xThrow * controlRollFactor ;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }


    private void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * controlSpeed * Time.deltaTime;
        float rawNewXPos = transform.localPosition.x + xOffset;
        float clampedXpos = Mathf.Clamp(rawNewXPos, -xRange, xRange);

        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThrow * controlSpeed * Time.deltaTime;
        float rawNewyPos = transform.localPosition.y + yOffset;
        float clampedypos = Mathf.Clamp(rawNewyPos, yMin, yMax);

        transform.localPosition = new Vector3(clampedXpos, clampedypos, transform.localPosition.z);
    }

    void ProcessFireing()
    {
        if (CrossPlatformInputManager.GetButton("Fire1"))
        {
            SetGunsActive(true);
        }
        else
        {
            SetGunsActive(false);
        }
    }

    private void SetGunsActive(bool isActive)
    {
        
        foreach (GameObject guns in guns)//care may affect death Fx
        {
            var emissionModule = guns.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = isActive;
        }
    }




}
